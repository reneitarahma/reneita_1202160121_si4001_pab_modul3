package com.example.reneita_1202160121_si4001_pab_modul3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Spinner;

public class detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ((TextView)findViewById(R.id.name1)).setText(getIntent().getStringExtra("nama"));
        ((TextView)findViewById(R.id.pkj2)).setText(getIntent().getStringExtra("job"));
        if (getIntent().getStringExtra("jeniskelamin").equals("Male")){
            ((ImageView)findViewById(R.id.female1)).setImageResource(R.drawable.male);
        }else {
            ((ImageView)findViewById(R.id.female1)).setImageResource(R.drawable.female);
        }
    }
}
